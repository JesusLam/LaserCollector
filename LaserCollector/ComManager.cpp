#include "stdafx.h"
#include "ComManager.h"


CComManager::CComManager()
{
}


CComManager::~CComManager()
{
}

//创建串口
HANDLE CComManager::CreateCom(std::string comName)
{
	HANDLE comHandle = CreateFileA(comName.c_str(),
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		0,
		NULL);
	if (comHandle == (HANDLE)-1)
	{
		int nResult = 0;
		nResult = GetLastError();
		return (HANDLE)-1;
	}
	COMMTIMEOUTS timeOuts;
	memset(&timeOuts, 0, sizeof(COMMTIMEOUTS));
	timeOuts.ReadIntervalTimeout = 50;
	timeOuts.ReadTotalTimeoutConstant = 0;
	timeOuts.ReadTotalTimeoutMultiplier = 0;
	timeOuts.WriteTotalTimeoutConstant = 50;
	timeOuts.WriteTotalTimeoutMultiplier = 0;
	SetCommTimeouts(comHandle, &timeOuts);
	DCB comDcb;
	memset(&comDcb, 0, sizeof(DCB));
	GetCommState(comHandle, &comDcb);
	comDcb.ByteSize = 8;
	SetCommState(comHandle, &comDcb);
	return comHandle;
}

int CComManager::ExchangeMes(HANDLE comHandle, std::string sendMsg, std::string & recvMsg, std::string endStr)
{
	DWORD comError = 0;
	ClearCommError(comHandle, &comError, NULL);
	PurgeComm(comHandle, PURGE_TXCLEAR | PURGE_RXCLEAR);
	DWORD realNum = 0;
	int nResult = WriteFile(comHandle, sendMsg.c_str(), sendMsg.size(), &realNum, NULL);
	if (nResult == 0)
	{
		int nError = GetLastError();

	}
	realNum = 0;
	while (recvMsg.find(endStr) == std::string::npos)
	{
		char recvBuf[1024] = { 0 };
		if (ReadFile(comHandle, recvBuf, 1024, &realNum, NULL) == 0)
			return -1;
		recvMsg += recvBuf;
	}
	return 0;
}
//读取获取的数据
int CComManager::RecvMes(HANDLE comHandle, std::string & recvMsg, std::string endStr)
{
	DWORD comError = 0;
	ClearCommError(comHandle, &comError, NULL);
	PurgeComm(comHandle, PURGE_TXCLEAR | PURGE_RXCLEAR);
	DWORD realNum = 0;
	while (recvMsg.find(endStr) == std::string::npos)
	{
		char recvBuf[1024] = { 0 };
		if (ReadFile(comHandle, recvBuf, 1024, &realNum, NULL) == 0)
		{
			int nResult = GetLastError();
			return -1;
		}
		recvMsg += recvBuf;
	}
	return 0;
}
//关闭串口
int CComManager::CloseCom(HANDLE comHandle)
{
	CloseHandle(comHandle);
	return 0;
}

int CComManager::Init(CRunLogic * pRunLogic)
{
	m_pRunLogic = pRunLogic;
	return 0;
}