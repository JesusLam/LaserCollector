#include "StdAfx.h"
#include "Dog.h"


CDog::CDog(void)
{
	m_hDogModule=LoadLibrary(_T("dog_lib.dll"));
//	m_pCheckDog=m_pCheckDogExist=m_pCheckFunID=NULL;
	if (m_hDogModule!=NULL)
	{
		m_pCheckDogExist=(CheckDogExistFun)GetProcAddress(m_hDogModule,(LPCSTR) (MAKELONG(1,0)));//"CheckDogExist");
		m_pCheckDog=(CheckDogFun)GetProcAddress(m_hDogModule,(LPCSTR) (MAKELONG(2,0)));//"CheckDog");
		m_pCheckFunID=(CheckFunIDFun)GetProcAddress(m_hDogModule,(LPCSTR) (MAKELONG(3,0)));//"CheckFunID");

		if (m_pCheckFunID==NULL||m_pCheckDog==NULL||m_pCheckDogExist==NULL)
		{
			FreeLibrary(m_hDogModule);
			m_hDogModule=NULL;
		}
	}
	
	
}


CDog::~CDog(void)
{
	if (m_hDogModule!=NULL)
	{
		FreeLibrary(m_hDogModule);
		m_hDogModule=NULL;
	}
}
