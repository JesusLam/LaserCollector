#include "stdafx.h"
#include "RunLogic.h"
#include "stdio.h"

UINT __stdcall RunThread(__in  LPVOID lpParameter)
{
	CRunLogic *ptrLogic = (CRunLogic*)lpParameter;
	int nLaserType = 0;

	HANDLE comHandle = ptrLogic->GetComHandle();
	std::string strRecvMes;
	std::vector <std::string> strResult;
	while (true && ptrLogic->IsThreadRun() != 0)
	{
		nLaserType = ptrLogic->GetSystemConfig()->GetLaserType();
		CString cstMsg;
		strRecvMes.clear();
		strResult.clear();
		//接收与解析
		strResult = ptrLogic->AnalysisProtocol(nLaserType, comHandle, strRecvMes);
		//显示测量结果
		for (int i = 0; i < strResult.size(); i++)
		{
			cstMsg = strResult[i].c_str();
			std::string tempCsv;
			tempCsv = CT2A(cstMsg + _T("J"));
			COutputLog::OutputCsv(tempCsv);
			ptrLogic->UpdateLog(_T("当前能量为： ") + cstMsg + _T("J"));
		}
	}
	return 0;
}

CRunLogic::CRunLogic()
{
}


CRunLogic::~CRunLogic()
{
}

int CRunLogic::InitLogic(HWND mainDlg)
{
	m_comManager.Init(this);
	m_mainDlg = mainDlg;
	return 0;
}

int CRunLogic::ThreadBegin()
{
	m_RunThreadHandle = (HANDLE)_beginthreadex(NULL, 0, RunThread, this, 0, NULL);

	bIsThreadRun = 1;
	return 0;
}

int CRunLogic::ThreadStop()
{
	TerminateThread(GetRunThreadHandle(), 0);
	bIsThreadRun = 0;
	CloseHandle(m_RunThreadHandle);
	UpdateLog(_T("已停止运行！"));
	return 0;
}

int CRunLogic::ComInit()
{
	std::string comName = CTool::ComNumSettingToCode(GetSystemConfig()->GetComNum());
	//创建名为comName的串口
	m_comHandle = GetComManager()->CreateCom(comName);
	if (m_comHandle == (HANDLE)-1)
	{
		UpdateLog(_T("串口连接失败，请检查串口设置参数！"));
		return -1;
	}
	DCB comDCB;
	memset(&comDCB, 0, sizeof(DCB));
	GetCommState(m_comHandle, &comDCB);
	//获取串口的参数
	comDCB.BaudRate = CTool::BaudRateSettingToCode(GetSystemConfig()->GetBaudRate());
	comDCB.fParity = GetSystemConfig()->GetParityEnable();
	comDCB.Parity = CTool::ParityTypeSettingToCode(GetSystemConfig()->GetParity());
	comDCB.StopBits = CTool::StopBitSettingToCode(GetSystemConfig()->GetStopBit());
	SetCommState(m_comHandle, &comDCB);
	UpdateLog(_T("串口连接成功！"));
	return 0;
}

BOOL CRunLogic::IsLaserTypeChange(CString cstLaserType)
{
	if (GetSystemConfig()->GetLaserType() == CTool::GetMachineTypeIndex(cstLaserType))
		return 0;
	return 1;
}

std::vector <std::string> CRunLogic::AnalysisProtocol(int nLaserType, HANDLE comHandle, std::string strOriginal)
{
	std::string strEnd;
	std::string strTemp;
	std::string strResult;
	std::vector <std::string> vecResultStr;
	std::vector <double> vecdbResult;
	double dbEnergyPara = 0.0;
	BOOL bEnableHighSpeed = 0;
	bEnableHighSpeed = GetSystemConfig()->GetEnableHighSpeed();
	dbEnergyPara = GetEnergyPara(nLaserType);
	if (bEnableHighSpeed)
	{
		strOriginal.clear();
		strEnd = 0x42;
		GetComManager()->RecvMes(comHandle, strOriginal, strEnd);
		strTemp = strOriginal.substr(3, 4);
		vecdbResult.push_back(HighSpeedAnalyze(strTemp));
		vecResultStr.push_back(CTool::DoubleToString(vecdbResult[0]));
	}
	else
	{
		strOriginal.clear();
		strEnd = 0x03;
		GetComManager()->RecvMes(comHandle, strOriginal, strEnd);
		int nPointCount = 0;
		int npos = 0;
		npos = strOriginal.find_last_of("\x3") - 9;
		strTemp = strOriginal.substr(7, npos);
		nPointCount = (strTemp.length()) / 8;
		for (int i = 0; i < nPointCount; i++)
		{
			strTemp = strOriginal.substr((7 + 8 * i), 8);
			strResult = strTemp.substr(2, 6);
			vecdbResult.push_back(StandardAnalyze(strResult, dbEnergyPara));
			vecResultStr.push_back(CTool::DoubleToString(vecdbResult[i]));
		}
	}
	return vecResultStr;
}

double CRunLogic::HighSpeedAnalyze(std::string strOriginal)
{
	double dbResult = 0;
	unsigned int nTemp = 0;
	nTemp = CTool::StringToUnsignedInt(strOriginal, ((strOriginal.length())/2));
	dbResult = nTemp / 100.0;
	return dbResult;
}

double CRunLogic::StandardAnalyze(std::string strTemp, double dbEnergyPara)
{
	unsigned int nTemp = 0;
	double dbResult = 0;
	nTemp = CTool::StringToUnsignedInt(strTemp, ((strTemp.length()) / 2));
	dbResult = nTemp / dbEnergyPara;
	return dbResult;
}

double CRunLogic::GetEnergyPara(int nLaserType)
{
	double dbPara = 0.0;
	if (nLaserType == 0)
		dbPara = 502285.0/30.0;
	else if (nLaserType == 1)
		dbPara = 505045.0 / 30.0;
	else if (nLaserType == 2)
		dbPara = 506500.0 / 50.0;
	else if (nLaserType == 3)
		dbPara = 507259.0 / 30.0;
	else if (nLaserType == 4)
		dbPara = 505900.0 / 50.0;
	else if (nLaserType == 5)
		dbPara = 491658.0 / 30.0;
	else if (nLaserType == 6)
		dbPara = 491658.0 / 30.0;
	else if (nLaserType == 7)
		dbPara = 505705.0 / 80.0;
	else if (nLaserType == 8)
		dbPara = 507259.0 / 40.0;
	else if (nLaserType == 9)
		dbPara = 507259.0 / 45.0;
	else if (nLaserType == 10)
		dbPara = 507259.0 / 60.0;
	else if (nLaserType == 11)
		dbPara = 507259.0 / 45.0;
	else if (nLaserType == 12)
		dbPara = 507259.0 / 60.0;
	else if (nLaserType == 13)
		dbPara = 507259.0 / 60.0;
	else if (nLaserType == 14)
		dbPara = 507259.0 / 90.0;
	else if (nLaserType == 15)
		dbPara = 518000.0 / 120.0;
	else if (nLaserType == 16)
		dbPara = 505705.0 / 50.0;
	else if (nLaserType == 17)
		dbPara = 505705.0 / 35.0;
	else if (nLaserType == 18)
		dbPara = 507259.0 / 60.0;
	else if (nLaserType == 19)
		dbPara = 518000.0 / 90.0;
	else if (nLaserType == 20)
		dbPara = 511200.0 / 90.0;
	else if (nLaserType == 21)
		dbPara = 511200.0 / 120.0;
	else if (nLaserType == 22)
		dbPara = 518000.0 / 120.0;
	return dbPara;
}

int CRunLogic::UpdateLog(CString log)
{
	m_log = log;
	std::string tempLog;
	tempLog = CT2A(m_log);
	COutputLog::OutputLog(tempLog);
	SendMessage(m_mainDlg, UM_UPDATE_LOG, NULL, NULL);
	return 0;
}

BOOL CRunLogic::IsThreadRun()
{
	return bIsThreadRun;
}

CTcpManager * CRunLogic::GetTcpManager()
{
	return &m_tcpManager;
}

CComManager * CRunLogic::GetComManager()
{
	return &m_comManager;
}

CSystemConfig * CRunLogic::GetSystemConfig()
{
	return &m_systemConfig;
}

HANDLE CRunLogic::GetComHandle()
{
	return m_comHandle;
}

HANDLE CRunLogic::GetRunThreadHandle()
{
	return m_RunThreadHandle;
}

HWND * CRunLogic::GetMainDlg()
{
	return &m_mainDlg;
}

CString CRunLogic::GetLOG()
{
	return m_log;
}
