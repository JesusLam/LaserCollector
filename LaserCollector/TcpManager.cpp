#include "stdafx.h"
#include "TcpManager.h"
#include "RunLogic.h"
#include <WS2tcpip.h>
#include <winerror.h>


CTcpManager::CTcpManager()
	: m_pRunLogic(nullptr)
{
	int iResult = NO_ERROR;
	iResult = WSAStartup(MAKEWORD(2, 2), &m_WsaData);
	if (iResult != NO_ERROR)
	{
		throw std::exception("请按以下步骤进行，开始->运行->cmd->netsh winsock reset->回车->重启电脑");
	}
}


CTcpManager::~CTcpManager()
{
	WSACleanup();
}

SOCKET CTcpManager::CreateSocket(SOCKETTYPE socketType)
{
	SOCKET tempSocket = INVALID_SOCKET;
	switch (socketType)
	{
	case CTcpManager::TCP_CLIENT:
		tempSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		break;
	default:
		break;
	}
	linger ling;
	ling.l_onoff = 1;
	ling.l_linger = 0;
	setsockopt(tempSocket, SOL_SOCKET, SO_LINGER, (char *)&ling, sizeof(linger));
	int timeOut = 0;
	setsockopt(tempSocket, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeOut, sizeof(int));
	setsockopt(tempSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeOut, sizeof(int));
	return tempSocket;
}

int CTcpManager::Bind(SOCKET socketClient, std::string addr, int port)
{
	sockaddr_in sockaddrClient;
	memset(&sockaddrClient, 0, sizeof(sockaddr_in));
	in_addr tempAdrr;
	InetPton(AF_INET, CString(addr.c_str()), &tempAdrr);
	sockaddrClient.sin_family = AF_INET;
	sockaddrClient.sin_addr.s_addr = tempAdrr.S_un.S_addr;
	sockaddrClient.sin_port = htons(port);
	int error = -1;
	if (bind(socketClient, (SOCKADDR*)&sockaddrClient, sizeof(SOCKADDR)) == -1)
	{
		error = WSAGetLastError();
		return -1;
	}
	return 0;
}

int CTcpManager::Connect(SOCKET socketClient, std::string addr, int port)
{
	sockaddr_in sockaddrClient;
	memset(&sockaddrClient, 0, sizeof(sockaddr_in));
	in_addr tempAdrr;
	InetPton(AF_INET, CString(addr.c_str()), &tempAdrr);
	sockaddrClient.sin_family = AF_INET;
	sockaddrClient.sin_addr.s_addr = tempAdrr.S_un.S_addr;
	sockaddrClient.sin_port = htons(port);
	unsigned long ul = 1;
	ioctlsocket(socketClient, FIONBIO, &ul);
	int ret = -1;
	int error = -1;
	if (connect(socketClient, (SOCKADDR*)&sockaddrClient, sizeof(sockaddrClient)) == -1)
	{
		error = WSAGetLastError();
		timeval tm;
		tm.tv_sec = 2;
		tm.tv_usec = 0;
		fd_set set;
		FD_ZERO(&set);
		FD_SET(socketClient, &set);
		if (select(socketClient, NULL, &set, NULL, &tm) <= 0)
		{
			error = WSAGetLastError();
			ret = -1;
		}
		else
			ret = 0;
	}
	else
		ret = 0;
	if (ret == -1)
		CloseSocket(socketClient);
	ul = 0;
	ioctlsocket(socketClient, FIONBIO, &ul);
	return ret;
}

int CTcpManager::CloseSocket(SOCKET socketClient)
{
	closesocket(socketClient);
	return 0;
}

int CTcpManager::TcpSend(SOCKET socketClient, std::string sendMes)
{
	int ret = -1;
	ret = send(socketClient, sendMes.c_str(), sendMes.size() + 1, 0);
	if (ret == SOCKET_ERROR)
		ret = -1;
	else
		ret = 0;
	return ret;
}

int CTcpManager::TcpRecv(SOCKET socketClient, std::string & recvString, std::string endStr)
{
	while (recvString.find(endStr) == std::string::npos)
	{
		char recvBuf[1024] = { 0 };
		if (recv(socketClient, recvBuf, 1024, 0) <= 0)
			return -1;
		recvString += recvBuf;
	}
	return 0;
}

int CTcpManager::OnceTcpRecv(SOCKET socketClient, std::string & recvString)
{
	char recvBuf[1024] = { 0 };
	if (recv(socketClient, recvBuf, 1024, 0) <= 0)
		return -1;
	recvString = recvBuf;
	return 0;
}

int CTcpManager::ExchangeMes(SOCKET & clientSocket, std::string sendMsg, std::string & recvMsg, std::string endStr, std::string ip, int port)
{
	ClearRecv(clientSocket);
	if (TcpSend(clientSocket, sendMsg) == -1)
	{
		int error = -1;
		error = WSAGetLastError();
		CloseSocket(clientSocket);
		clientSocket = CreateSocket(CTcpManager::SOCKETTYPE::TCP_CLIENT);
		if (Connect(clientSocket, ip, port) == -1)
		{
			return -1;
		}
		else
		{
			if (TcpSend(clientSocket, sendMsg) == -1)
				return -1;
		}
	}
	if (TcpRecv(clientSocket, recvMsg, endStr) == -1)
	{
		return -1;
	}
	return 0;
}

int CTcpManager::Init(CRunLogic * pRunLogic)
{
	m_pRunLogic = pRunLogic;
	return 0;
}

int CTcpManager::ClearRecv(SOCKET & clientSocket)
{
	timeval tmOut;
	tmOut.tv_sec = 0;
	tmOut.tv_usec = 0;
	fd_set fd;
	FD_ZERO(&fd);
	FD_SET(clientSocket, &fd);
	int nRet = 0;
	char temp[2] = { 0 };
	while (1)
	{
		nRet = select(FD_SETSIZE, &fd, NULL, NULL, &tmOut);
		if (nRet == 0)
		{
			break;
		}
		if (recv(clientSocket, temp, 1, 0) <= 0)
			return -1;
	}
	return 0;
}