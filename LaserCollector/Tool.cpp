#include "stdafx.h"
#include "Tool.h"
#include <sstream>
#include <iostream>


CTool::CTool()
{
}


CTool::~CTool()
{
}

int CTool::GetMachineTypeIndex(CString csType)
{
	int ret = 0;
	if (csType == "PB25")
		ret = 0;
	else if(csType == "PB25CE")
		ret = 1;
	else if (csType == "PB50")
		ret = 2;
	else if (csType == "PB50A")
		ret = 3;
	else if (csType == "PB50CE")
		ret = 4;
	else if (csType == "PB50ACE")
		ret = 5;
	else if (csType == "PB50BCE")
		ret = 6;
	else if (csType == "PB80")
		ret = 7;
	else if (csType == "PB300CE-18")
		ret = 8;
	else if (csType == "PB300CE-25")
		ret = 9;
	else if (csType == "PB300CE-30")
		ret = 10;
	else if (csType == "PB300CE-45")
		ret = 11;
	else if (csType == "PB300CE-60")
		ret = 12;
	else if (csType == "PB300CE-90")
		ret = 13;
	else if (csType == "PB300CE-9kW")
		ret = 14;
	else if (csType == "PB300CE-12kW")
		ret = 15;
	else if (csType == "WF80")
		ret = 16;
	else if (csType == "WF80-7")
		ret = 17;
	else if (csType == "WF310")
		ret = 18;
	else if (csType == "WF600-50")
		ret = 19;
	else if (csType == "WF600-50A")
		ret = 20;
	else if (csType == "WF600-90A")
		ret = 21;
	else if (csType == "WF600-90/100")
		ret = 22;
	return ret;
}

CString CTool::GetMachineTypeString(int m_nType)
{
	CString csType;
	if (m_nType == 0)
		csType = "PB25";
	else if (m_nType == 1)
		csType = "PB25CE";
	else if (m_nType == 2)
		csType = "PB50";
	else if (m_nType == 3)
		csType = "PB50A";
	else if (m_nType == 4)
		csType = "PB50CE";
	else if (m_nType == 5)
		csType = "PB50ACE";
	else if (m_nType == 6)
		csType = "PB50BCE";
	else if (m_nType == 7)
		csType = "PB80";
	else if (m_nType == 8)
		csType = "PB300CE-18";
	else if (m_nType == 9)
		csType = "PB300CE-25";
	else if (m_nType == 10)
		csType = "PB300CE-30";
	else if (m_nType == 11)
		csType = "PB300CE-45";
	else if (m_nType == 12)
		csType = "PB300CE-60";
	else if (m_nType == 13)
		csType = "PB300CE-90";
	else if (m_nType == 14)
		csType = "PB300CE-9kW";
	else if (m_nType == 15)
		csType = "PB300CE-12kW";
	else if (m_nType == 16)
		csType = "WF80";
	else if (m_nType == 17)
		csType = "WF80-7";
	else if (m_nType == 18)
		csType = "WF310";
	else if (m_nType == 19)
		csType = "WF600-50";
	else if (m_nType == 20)
		csType = "WF600-50A";
	else if (m_nType == 21)
		csType = "WF600-90A";
	else if (m_nType == 22)
		csType = "WF600-90/100";
	return csType;
}

CString CTool::IntToCString(int nValue)
{
	CString csTemp;
	csTemp.Format(_T("%d"), nValue);
	return csTemp;
}

int CTool::CStringToInt(CString csStr)
{

	return atoi(CT2A(csStr));
}

std::string CTool::ComNumSettingToCode(int nIndex)
{
	std::string temp = "COM";
	std::ostringstream os;
	os << temp << nIndex + 1;
	temp = os.str();
	return temp;
}

std::string CTool::DoubleToString(double dbIndex)
{
	std::string res;
	std::stringstream ss;
	ss << dbIndex;
	ss >> res;
	return res;
}

unsigned long CTool::BaudRateSettingToCode(int nIndex)
{
	int baudRate = 0;
	switch (nIndex)
	{
	case 0:
		baudRate = CBR_9600;
		break;
	case 1:
		baudRate = CBR_14400;
		break;
	case 2:
		baudRate = CBR_19200;
		break;
	case 3:
		baudRate = CBR_38400;
		break;
	case 4:
		baudRate = CBR_57600;
		break;
	case 5:
		baudRate = CBR_115200;
		break;
	default:
		break;
	}
	return baudRate;
}

unsigned long CTool::ParityTypeSettingToCode(int nIndex)
{
	int parityType = 0;
	switch (nIndex)
	{
	case 0:
		parityType = NOPARITY;
		break;
	case 1:
		parityType = ODDPARITY;
		break;
	case 2:
		parityType = EVENPARITY;
		break;
	case 3:
		parityType = MARKPARITY;
		break;
	default:
		break;
	}
	return parityType;
}

unsigned long CTool::StopBitSettingToCode(int nIndex)
{
	int stopBit = 0;
	switch (nIndex)
	{
	case 0:
		stopBit = ONESTOPBIT;
		break;
	case 1:
		stopBit = ONE5STOPBITS;
		break;
	case 2:
		stopBit = TWOSTOPBITS;
		break;
	default:
		break;
	}
	return stopBit;
}

unsigned int CTool::StringToUnsignedInt(std::string strTemp, int nByteAmount)
{
	unsigned int nTemp = 0;
	double dbResult = 0;
	for (int i = 0; i < strTemp.size(); i++)
		strTemp[i] &= 0x0F;
	char tran[4] = { 0 };
	int index = 0;
	for (int i = 0; i < ( 2 * nByteAmount); i += 2)
		tran[index++] = (strTemp[i] << 4) | strTemp[i + 1];
	index = 0;
	for (int i = (nByteAmount - 1); i >= 0; i--)
		nTemp |= (((unsigned int)tran[index++]) & 0x000000ff) << 8 * i;
	return nTemp;
}
