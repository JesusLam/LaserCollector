#pragma once
#include "afxcmn.h"
#include "TcpManager.h"
#include "ComManager.h"
#include "SystemConfig.h"
#include "Tool.h"
#include "OutputLog.h"
#include "MessageID.h"
#include <string>
#include <iomanip>
#include <vector>

UINT __stdcall RunThread(__in  LPVOID lpParameter);

class CRunLogic
{
public:
	CRunLogic();
	~CRunLogic();
public:
	int InitLogic(HWND mainDlg);
	int ThreadBegin();
	int ThreadStop();
	int ComInit();
	BOOL IsLaserTypeChange(CString cstLaserType);
public:
	//接收
	std::vector <std::string> AnalysisProtocol(int nLaserType, HANDLE comHandle, std::string strOriginal);
	//高速分光解析
	double HighSpeedAnalyze(std::string strOriginal);
	//标准解析
	double StandardAnalyze(std::string strOriginal, double dbEnergyPara);
	//显示结果
	double GetEnergyPara(int nLaserType);
	int UpdateLog(CString log);
	BOOL IsThreadRun();
public:
	CTcpManager * GetTcpManager();
	CComManager * GetComManager();
	CSystemConfig * GetSystemConfig();
	HANDLE GetComHandle();
	HANDLE GetRunThreadHandle();
	HWND * GetMainDlg();
	CString GetLOG();
private:
	CTcpManager m_tcpManager;
	CComManager m_comManager;
	CSystemConfig m_systemConfig;
	SOCKET m_tcpSocket;
	HWND m_mainDlg;
	HANDLE m_comHandle;
	HANDLE m_RunThreadHandle;
	CString m_log;
	BOOL bIsThreadRun;
};

