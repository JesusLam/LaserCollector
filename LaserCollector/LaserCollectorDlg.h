#include "RunLogic.h"
#include "SettingDlg.h"
#include "afxwin.h"
#include "afxcmn.h"
// LaserCollectorDlg.h : 头文件
//

#pragma once


// CLaserCollectorDlg 对话框
class CLaserCollectorDlg : public CDialogEx
{
// 构造
public:
	CLaserCollectorDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_LASERCOLLECTOR_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

// 实现
protected:
	HICON m_hIcon;
	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg LRESULT OnUmUpdateLog(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonSetting();
	afx_msg void OnBnClickedButtonRun();
	afx_msg void OnBnClickedButtonStop();
	int EnableButton();
public:
	CEdit m_editLOG;
	CString m_log;
private:
	CRunLogic m_cRunLogic;
public:
	afx_msg void OnBnClickedButtonClearLog();
};
