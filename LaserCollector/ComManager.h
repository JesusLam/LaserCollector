#pragma once
#include <WinSock2.h>
#include <string>
class CRunLogic;

class CComManager
{
public:
	CComManager();
	~CComManager();
public:
	HANDLE CreateCom(std::string comName);
	int ExchangeMes(HANDLE comHandle, std::string sendMsg, std::string & recvMsg, std::string endStr);
	int RecvMes(HANDLE comHandle, std::string & recvMsg, std::string endStr);
	int CloseCom(HANDLE comHandle);
	int Init(CRunLogic * pLogicHI);
private:
	WSADATA m_WsaData;
	CRunLogic * m_pRunLogic;
};

