#include "stdafx.h"
#include "SystemConfig.h"


CSystemConfig::CSystemConfig()
{
	strcpy_s(m_szConfigFile, ".\\LaserCollectorConfig.ini");
}


CSystemConfig::~CSystemConfig()
{
}

void CSystemConfig::GetString(const char * szAppName, const char * szKeyName, const char * szDefault, char * szDst)
{
	GetPrivateProfileStringA(szAppName, szKeyName, szDefault, szDst, MAX_PATH, m_szConfigFile);
	WritePrivateProfileStringA(szAppName, szKeyName, szDst, m_szConfigFile);
}

void CSystemConfig::WriteString(const char * szAppName, const char * szKeyName, const char * szWrite)
{
	WritePrivateProfileStringA(szAppName, szKeyName, szWrite, m_szConfigFile);
}

int CSystemConfig::GetInteger(const char * szAppName, const char * szKeyName, int nDefault)
{
	char szTemp[MAX_PATH] = { 0 };
	char szDefault[MAX_PATH] = { 0 };
	sprintf_s(szDefault, "%d", nDefault);
	GetString(szAppName, szKeyName, szDefault, szTemp);
	return atoi(szTemp);
}

void CSystemConfig::SetInteger(const char * szAppName, const char * szKeyName, int nSrc)
{
	char szTemp[MAX_PATH] = { 0 };
	sprintf_s(szTemp, "%d", nSrc);
	WriteString(szAppName, szKeyName, szTemp);
}

double CSystemConfig::GetDouble(const char * szAppName, const char * szKeyName, double dbDefault)
{
	char szTemp[MAX_PATH] = { 0 };
	char szDefault[MAX_PATH] = { 0 };
	sprintf_s(szDefault, "%lf", dbDefault);
	GetString(szAppName, szKeyName, szDefault, szTemp);
	return atof(szTemp);
}

void CSystemConfig::SetDouble(const char * szAppName, const char * szKeyName, double dbSrc)
{
	char szTemp[MAX_PATH] = { 0 };
	sprintf_s(szTemp, "%lf", dbSrc);
	WriteString(szAppName, szKeyName, szTemp);
}


int CSystemConfig::SetComNum(int nValue)
{
	SetInteger("Comm", "Com", nValue);
	return 0;
}

int CSystemConfig::GetComNum()
{
	return GetInteger("Comm", "Com", 0);
}

int CSystemConfig::SetBaudRate(int nValue)
{
	SetInteger("Comm", "BaudRate", nValue);
	return 0;
}

int CSystemConfig::GetBaudRate()
{
	return GetInteger("Comm", "BaudRate", 9600);
}

int CSystemConfig::SetParityEnable(int nValue)
{
	SetInteger("Comm", "ParityEnable", nValue);
	return 0;
}

int CSystemConfig::GetParityEnable()
{
	return GetInteger("Comm", "ParityEnable", 0);
}

int CSystemConfig::SetParity(int nValue)
{
	SetInteger("Comm", "ParityType", nValue);
	return 0;
}

int CSystemConfig::GetEnableHighSpeed()
{
	return GetInteger("Comm", "EnableHighSpeed", 0);
}

int CSystemConfig::SetEnableHighSpeed(int nValue)
{
	SetInteger("Comm", "EnableHighSpeed", nValue);
	return 0;
}

int CSystemConfig::GetParity()
{
	return GetInteger("Comm", "ParityType", 0);
}

int CSystemConfig::SetStopBit(int nValue)
{
	SetInteger("Comm", "StopBit", nValue);
	return 0;
}

int CSystemConfig::GetStopBit()
{
	return GetInteger("Comm", "StopBit", 0);
}

int CSystemConfig::SetLaserType(int nValue)
{
	SetInteger("Comm", "LaserType", nValue);
	return 0;
}

int CSystemConfig::GetLaserType()
{
	return GetInteger("Comm", "LaserType", 0);
}
