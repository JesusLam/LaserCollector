#pragma once
#include "afxwin.h"
#include "RunLogic.h"
#include "Tool.h"

// CSettingDlg 对话框

class CSettingDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSettingDlg)

public:
	CSettingDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CSettingDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_SETTING };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
public:
	int SetLogic(CRunLogic * ptr);

private:
	CRunLogic * m_pRunLogic;
public:
	CString m_csLaserType;
	BOOL m_bParityEnable;
	BOOL m_bEnableHighSpeed;
	int m_nComNum;
	int m_nBaudRate;
	int m_nStopBit;
	int m_nParity;
};
