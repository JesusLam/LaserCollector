#pragma once
#include <fstream>
#include <string>

class COutputLog
{
public:
	COutputLog();
	~COutputLog();
	static int OutputLog(std::string stLog);
	static int OutputCsv(std::string stLog);
private:
	static int CreateLogDir();
private:
	static std::string m_stFileLogPath;
};
