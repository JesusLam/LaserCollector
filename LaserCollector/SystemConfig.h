#pragma once
#include <string>
class CSystemConfig
{
public:
	CSystemConfig();
	~CSystemConfig();
public:
	void GetString(const char* szAppName, const char* szKeyName, const char* szDefault, char* szDst);
	void WriteString(const char* szAppName, const char* szKeyName, const char* szWrite);
	int GetInteger(const char* szAppName, const char* szKeyName, int nDefault);
	void SetInteger(const char* szAppName, const char* szKeyName, int nSrc);
	double GetDouble(const char* szAppName, const char* szKeyName, double dbDefault);
	void SetDouble(const char* szAppName, const char* szKeyName, double dbSr);
public:
	int SetComNum(int nValue);
	int GetComNum();
	//波特率
	int SetBaudRate(int nValue);
	int GetBaudRate();
	//奇偶启用
	int SetParityEnable(int nValue);
	int GetParityEnable();
	//奇偶校验
	int SetParity(int nValue);
	int GetParity();
	//停止位位数
	int SetStopBit(int nValue);
	int GetStopBit();
	//激光器类型
	int SetLaserType(int nValue);
	int GetLaserType();
	//高速分光
	int SetEnableHighSpeed(int nValue);
	int GetEnableHighSpeed();
private:
	char m_szConfigFile[MAX_PATH];
};

