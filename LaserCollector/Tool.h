#pragma once
#include <string>
class CTool
{
public:
	CTool();
	~CTool();
public:
	static int GetMachineTypeIndex(CString csType);
	static int CStringToInt(CString csStr);
	static CString GetMachineTypeString(int nType);
	static CString IntToCString(int nValue);
	static std::string ComNumSettingToCode(int nIndex);
	static std::string DoubleToString(double dbIndex);
	static unsigned long BaudRateSettingToCode(int nIndex);
	static unsigned long ParityTypeSettingToCode(int nIndex);
	static unsigned long StopBitSettingToCode(int nIndex);
	static unsigned int StringToUnsignedInt(std::string strTemp, int nByteAmount);
};

