// SettingDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "LaserCollector.h"
#include "SettingDlg.h"
#include "afxdialogex.h"


// CSettingDlg 对话框

IMPLEMENT_DYNAMIC(CSettingDlg, CDialogEx)

CSettingDlg::CSettingDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DIALOG_SETTING, pParent)
	, m_csLaserType(_T(""))
	, m_bParityEnable(FALSE)
	, m_nComNum(0)
	, m_nBaudRate(0)
	, m_nStopBit(0)
	, m_nParity(0)
	, m_bEnableHighSpeed(FALSE)
{

}

CSettingDlg::~CSettingDlg()
{
}

int CSettingDlg::SetLogic(CRunLogic * ptr)
{
	m_pRunLogic = ptr;
	return 0;
}

void CSettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_CBString(pDX, IDC_COMBO_LASER_TYPE, m_csLaserType);
	DDX_Check(pDX, IDC_CHECK_PARITY_ENABLE, m_bParityEnable);
	DDX_CBIndex(pDX, IDC_COMBO_COM_NUM, m_nComNum);
	DDX_CBIndex(pDX, IDC_COMBO_BAUD_RATE, m_nBaudRate);
	DDX_CBIndex(pDX, IDC_COMBO_STOP_BIT, m_nStopBit);
	DDX_CBIndex(pDX, IDC_COMBO_PARITY, m_nParity);
	DDX_Check(pDX, IDC_CHECK_ENABLE_HIGH_SPEED, m_bEnableHighSpeed);
}


BEGIN_MESSAGE_MAP(CSettingDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CSettingDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CSettingDlg 消息处理程序


BOOL CSettingDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	// 从SystemConfig里面读取设置
	m_csLaserType = CTool::GetMachineTypeString(m_pRunLogic->GetSystemConfig()->GetLaserType());
	m_nComNum = m_pRunLogic->GetSystemConfig()->GetComNum();
	m_nBaudRate = m_pRunLogic->GetSystemConfig()->GetBaudRate();
	m_nStopBit = m_pRunLogic->GetSystemConfig()->GetStopBit();
	m_nParity = m_pRunLogic->GetSystemConfig()->GetParity();
	m_bParityEnable = m_pRunLogic->GetSystemConfig()->GetParityEnable();
	m_bEnableHighSpeed = m_pRunLogic->GetSystemConfig()->GetEnableHighSpeed();
	if (m_bParityEnable == 0)
		GetDlgItem(IDC_COMBO_PARITY)->EnableWindow(FALSE);
	//奇偶校正没写
	UpdateData(FALSE);
	return 0;
}

void CSettingDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	//CDialogEx::OnOK();
	UpdateData(TRUE);
	if (m_pRunLogic->IsLaserTypeChange(m_csLaserType) == 1)
		m_pRunLogic->UpdateLog(_T("当前激光器类型:		") + m_csLaserType);
	m_pRunLogic->GetSystemConfig()->SetLaserType(CTool::GetMachineTypeIndex(m_csLaserType));
	m_pRunLogic->GetSystemConfig()->SetComNum(m_nComNum);
	m_pRunLogic->GetSystemConfig()->SetBaudRate(m_nBaudRate);
	m_pRunLogic->GetSystemConfig()->SetStopBit(m_nStopBit);
	m_pRunLogic->GetSystemConfig()->SetParityEnable(m_bParityEnable);
	m_pRunLogic->GetSystemConfig()->SetEnableHighSpeed(m_bEnableHighSpeed);
	m_pRunLogic->GetSystemConfig()->SetParity(m_nParity);
	//奇偶校正没写

}
