#pragma once
#include <WinSock2.h>
#include <string>
class CRunLogic;

class CTcpManager
{
public:
	CTcpManager();
	~CTcpManager();
public:
	enum SOCKETTYPE
	{
		TCP_CLIENT
	};
	SOCKET CreateSocket(SOCKETTYPE socketType);
	int Bind(SOCKET socketClient, std::string addr, int port);
	int Connect(SOCKET socketClient, std::string addr, int port);
	int CloseSocket(SOCKET socketClient);
	int TcpSend(SOCKET socketClient, std::string sendMes);
	int TcpRecv(SOCKET socketClient, std::string & recvString, std::string endStr);
	int OnceTcpRecv(SOCKET socketClient, std::string & recvString);
	int ExchangeMes(SOCKET & clientSocket, std::string sendMsg, std::string & recvMsg, std::string endStr, std::string ip, int port);
public:
	int Init(CRunLogic * pRunLogic);
private:
	int ClearRecv(SOCKET & clientSocket);
private:
	WSADATA m_WsaData;
	CRunLogic * m_pRunLogic;
};

