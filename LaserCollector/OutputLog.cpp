#include "stdafx.h"
#include "OutputLog.h"
#include <sstream>
#include <direct.h>
#include <io.h>

std::string COutputLog::m_stFileLogPath(".\\LOG\\FileLog.txt");

COutputLog::COutputLog()
{

}

COutputLog::~COutputLog()
{

}

int COutputLog::OutputLog(std::string stLog)
{
	CreateLogDir();
	CTime sys;
	sys = CTime::GetCurrentTime();
	char timebuf[1024] = { 0 };
	sprintf_s(timebuf, "[ %4d/%02d/%02d %02d:%02d:%02d ����%1d ]", sys.GetYear(), sys.GetMonth(), sys.GetDay(), sys.GetHour(),
		sys.GetMinute(), sys.GetSecond(), sys.GetDayOfWeek());
	std::string path;
	path = ".//LOG//";
	path += CT2A(sys.Format("%Y%m%d"));
	path += ".txt";
	std::fstream fs(path, std::ios::in | std::ios::out | std::ios::app);
	fs << timebuf;
	fs << stLog << std::endl;
	fs.close();
	return 0;
}

int COutputLog::OutputCsv(std::string stLog)
{
	CreateLogDir();
	CTime sys;
	sys = CTime::GetCurrentTime();
	char timebuf[1024] = { 0 };
	sprintf_s(timebuf, " %4d/%02d/%02d,%02d:%02d:%02d, ", sys.GetYear(), sys.GetMonth(), sys.GetDay(), sys.GetHour(),
		sys.GetMinute(), sys.GetSecond());
	std::string path;
	path = ".//LOG//";
	path += CT2A(sys.Format("%Y%m%d"));
	path += ".csv";
	std::fstream fs(path, std::ios::in | std::ios::out | std::ios::app);
	fs << timebuf;
	fs << stLog << std::endl;
	fs.close();
	return 0;
}

int COutputLog::CreateLogDir()
{
	if (_access("LOG", 6) == -1)
	{
		_mkdir("LOG");
	}
	return 0;
}

