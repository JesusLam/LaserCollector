#pragma once



typedef int(WINAPI *CheckDogFun)(LONG64 nCardID,char** ppstrMSG,int nStrSize);
typedef int(WINAPI *CheckDogExistFun)(char** ppstrMSG,int nStrSize);
typedef int(WINAPI *CheckFunIDFun)(int FunID,char** ppstrMSG,int nStrSize);


class CDog
{
public:
	
	CDog(void);
	~CDog(void);


	int  CheckDog(LONG64 nCardID,char** ppstrMSG,int nStrSize)
	{
		if (m_hDogModule==NULL)
		{
			return -1;
		}
		return m_pCheckDog(nCardID,ppstrMSG, nStrSize);
	}

	int  CheckDogExist(char** ppstrMSG,int nStrSize)
	{
		if (m_hDogModule==NULL)
		{
			return -1;
		}
		return m_pCheckDogExist(ppstrMSG, nStrSize);
	}

	int  CheckFunID(int FunID,char** ppstrMSG,int nStrSize)
	{
		if (m_hDogModule==NULL)
		{
			return -1;
		}
		return m_pCheckFunID(FunID,ppstrMSG, nStrSize);
	}
private:
	HINSTANCE m_hDogModule;
	CheckFunIDFun m_pCheckFunID;
	CheckDogExistFun m_pCheckDogExist;
	CheckDogFun m_pCheckDog;
};

